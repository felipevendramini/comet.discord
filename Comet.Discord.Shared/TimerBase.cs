﻿// =================================================================
// Comet.Discord.Bot Project
// 
// This project creates a Discord bot to communicate with Comet Conquer Online Emulator. For other
// related projects see:
// 
// Comet: https://gitlab.com/felipevendramini/comet
// Comet.Updater: https://gitlab.com/felipevendramini/comet.updater
// Comet.Discord: https://gitlab.com/felipevendramini/comet.discord
// 
// User: FELIPE VIEIRA VENDRAMINI [FELIPEVIEIRAVENDRAMI]
// 
// Comet.Discord - Comet.Discord.Shared - TimerBase.cs
// 
// Creation Date: 2021-03-19 16:37
// =================================================================

#region References

using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Discord;

#endregion

namespace Comet.Discord.Shared
{
    public abstract class TimerBase
    {
        private CancellationToken m_cancellationToken = CancellationToken.None;
        protected int m_interval = 1000;
        private string m_name;
        private Timer m_timer;

        protected TimerBase(int intervalMs, string name)
        {
            m_name = name;
            m_interval = intervalMs;

            m_timer = new Timer(TimerOnElapse, null, 0, intervalMs);
        }

        public string Name => m_name;

        public long ElapsedMilliseconds { get; private set; }

        public async Task StartAsync()
        {
            await OnStartAsync();
        }

        public Task CloseAsync()
        {
            m_cancellationToken = new CancellationToken(true);
            m_timer.Dispose(m_cancellationToken.WaitHandle);
            return OnCloseAsync();
        }

        private async void TimerOnElapse(object sender)
        {
            Stopwatch sw = new Stopwatch();
            if (!m_cancellationToken.IsCancellationRequested)
            {
                sw.Start();
                try
                {
                    await OnElapseAsync();
                    await Task.Delay(m_interval, m_cancellationToken);
                }
                catch (Exception ex)
                {
                    await Log.WriteLogAsync(LogSeverity.Critical, $"Error on thread {m_name}");
                    await Log.WriteLogAsync(LogSeverity.Debug, ex.ToString());
                }
                finally
                {
                    sw.Stop();
                    ElapsedMilliseconds = sw.ElapsedMilliseconds - m_interval;
                    sw.Reset();
                }
            }
        }

        public virtual async Task OnStartAsync()
        {
            await Log.WriteLogAsync(LogSeverity.Info, $"Timer [{m_name}] has started");
        }

        public virtual async Task<bool> OnElapseAsync()
        {
            await Log.WriteLogAsync(LogSeverity.Info, $"Timer [{m_name}] has elapsed at {DateTime.Now}");
            return true;
        }

        public virtual async Task OnCloseAsync()
        {
            await Log.WriteLogAsync(LogSeverity.Info, $"Timer [{m_name}] has finished");
        }
    }
}