﻿// =================================================================
// Comet.Discord.Bot Project
// 
// This project creates a Discord bot to communicate with Comet Conquer Online Emulator. For other
// related projects see:
// 
// Comet: https://gitlab.com/felipevendramini/comet
// Comet.Updater: https://gitlab.com/felipevendramini/comet.updater
// Comet.Discord: https://gitlab.com/felipevendramini/comet.discord
// 
// User: FELIPE VIEIRA VENDRAMINI [FELIPEVIEIRAVENDRAMI]
// 
// Comet.Discord - Comet.Discord.Shared - Log.cs
// 
// Creation Date: 2021-03-19 16:20
// =================================================================

#region References

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Discord;

#endregion

namespace Comet.Discord.Shared
{
    internal enum LogFolder
    {
        SystemLog,
        GameLog
    }

    internal struct LogFile
    {
        public LogFolder Folder;
        public string Path;
        public string Filename;
        public DateTime Date;
        public object Handle;
    }

    public static class Log
    {
        private static readonly Dictionary<string, LogFile> Files = new Dictionary<string, LogFile>();

        public static string DefaultFileName = "Discord";

        static Log()
        {
            RefreshFolders();
        }

        public static async Task WriteLogAsync(LogSeverity level, string message, params object[] values)
        {
            await WriteLogAsync(DefaultFileName, level, message, values);
        }

        public static async Task WriteLogAsync(string file, LogSeverity level, string message, params object[] values)
        {
            RefreshFolders();

            message = string.Format(message, values);
            message = $"{DateTime.Now:HH:mm:ss.fff} [{level}] - {message}";

            await WriteToFile(file, LogFolder.SystemLog, message);

            switch (level)
            {
                case LogSeverity.Critical:
                case LogSeverity.Error:
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                case LogSeverity.Warning:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;
                case LogSeverity.Info:
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
                case LogSeverity.Verbose:
                case LogSeverity.Debug:
                    Console.ForegroundColor = ConsoleColor.DarkGray;
                    break;
            }

            Console.WriteLine(message);
        }

        public static async Task GmLog(string file, string message, params object[] values)
        {
            RefreshFolders();

            if (values.Length > 0)
                message = string.Format(message, values);

            message = $"{DateTime.Now:HHmmss.fff} - {message}";

            await WriteToFile(file, LogFolder.GameLog, message);
        }

        private static async Task WriteToFile(string file, LogFolder eFolder, string value)
        {
            var now = DateTime.Now;
            if (!Files.TryGetValue(file, out var fileHandle))
                Files.Add(file, fileHandle = CreateHandle(file, eFolder));

            if (fileHandle.Date.Year != now.Year
                || fileHandle.Date.DayOfYear != now.DayOfYear)
            {
                fileHandle.Date = now;
                fileHandle.Path = $"{GetDirectory(eFolder)}{Path.DirectorySeparatorChar}{file}.log";
            }

            try
            {
                await using var fWriter = new FileStream(fileHandle.Path, FileMode.Append, FileAccess.Write,
                    FileShare.Write, 4096);
                await using var writer = new StreamWriter(fWriter);
                await writer.WriteLineAsync(value);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private static LogFile CreateHandle(string file, LogFolder folder)
        {
            if (Files.ContainsKey(file))
                return Files[file];

            var now = DateTime.Now;
            var logFile = new LogFile
            {
                Date = now,
                Filename = $"{now:YYYYMMdd)} - {file}.log",
                Handle = new object(),
                Path = $"{GetDirectory(folder)}{Path.DirectorySeparatorChar}{file}.log",
                Folder = folder
            };
            return logFile;
        }

        private static string GetDirectory(LogFolder folder)
        {
            var now = DateTime.Now;
            return string.Join(Path.DirectorySeparatorChar.ToString(), ".", $"{folder}", $"{now.Year:0000}",
                $"{now.Month:00}", $"{now.Day:00}");
        }

        private static void RefreshFolders()
        {
            foreach (var eVal in Enum.GetValues(typeof(LogFolder)).Cast<LogFolder>())
                if (!Directory.Exists(GetDirectory(eVal)))
                    Directory.CreateDirectory(GetDirectory(eVal));
        }
    }
}