﻿// =================================================================
// Comet.Discord.Bot Project
// 
// This project creates a Discord bot to communicate with Comet Conquer Online Emulator. For other
// related projects see:
// 
// Comet: https://gitlab.com/felipevendramini/comet
// Comet.Updater: https://gitlab.com/felipevendramini/comet.updater
// Comet.Discord: https://gitlab.com/felipevendramini/comet.discord
// 
// User: FELIPE VIEIRA VENDRAMINI [FELIPEVIEIRAVENDRAMI]
// 
// Comet.Discord - Comet.Discord.Shared - UnixTimestamp.cs
// 
// Creation Date: 2021-03-19 16:59
// =================================================================

#region References

using System;

#endregion

namespace Comet.Discord.Shared
{
    public static class UnixTimestamp
    {
        public static readonly DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime();

        public static DateTime ToDateTime(int timestamp)
        {
            return UnixEpoch.AddSeconds(timestamp);
        }

        public static int Now()
        {
            return Convert.ToInt32((DateTime.Now - new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime())
                .TotalSeconds);
        }

        public static int Timestamp(DateTime time)
        {
            return Convert.ToInt32((time - new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime()).TotalSeconds);
        }
    }
}