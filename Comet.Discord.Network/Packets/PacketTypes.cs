// =================================================================
// Comet.Discord.Bot Project
// 
// This project creates a Discord bot to communicate with Comet Conquer Online Emulator. For other
// related projects see:
// 
// Comet: https://gitlab.com/felipevendramini/comet
// Comet.Updater: https://gitlab.com/felipevendramini/comet.updater
// Comet.Discord: https://gitlab.com/felipevendramini/comet.discord
// 
// User: FELIPE VIEIRA VENDRAMINI [FELIPEVIEIRAVENDRAMI]
// 
// Comet.Discord - Comet.Discord.Network - PacketTypes.cs
// 
// Creation Date: 2021-03-19 16:14
// =================================================================

namespace Comet.Discord.Network.Packets
{
    /// <summary>
    ///     Packet types for the Conquer Online game client across all server projects.
    ///     Identifies packets by an unsigned short from offset 2 of every packet sent to
    ///     the server.
    /// </summary>
    public enum PacketType : ushort
    {
    }
}