// =================================================================
// Comet.Discord.Bot Project
// 
// This project creates a Discord bot to communicate with Comet Conquer Online Emulator. For other
// related projects see:
// 
// Comet: https://gitlab.com/felipevendramini/comet
// Comet.Updater: https://gitlab.com/felipevendramini/comet.updater
// Comet.Discord: https://gitlab.com/felipevendramini/comet.discord
// 
// User: FELIPE VIEIRA VENDRAMINI [FELIPEVIEIRAVENDRAMI]
// 
// Comet.Discord - Comet.Discord.Network - IPacket.cs
// 
// Creation Date: 2021-03-19 16:14
// =================================================================

namespace Comet.Discord.Network.Packets
{
    /// <summary>
    ///     Interface for packet encoding and decoding using TQ Digital Entertainment's byte
    ///     ordering rules. Called from the actor's send method to encode a packet without
    ///     needing to know the Client type for processing.
    /// </summary>
    public interface IPacket
    {
        void Decode(byte[] bytes);
        byte[] Encode();
    }
}