// =================================================================
// Comet.Discord.Bot Project
// 
// This project creates a Discord bot to communicate with Comet Conquer Online Emulator. For other
// related projects see:
// 
// Comet: https://gitlab.com/felipevendramini/comet
// Comet.Updater: https://gitlab.com/felipevendramini/comet.updater
// Comet.Discord: https://gitlab.com/felipevendramini/comet.discord
// 
// User: FELIPE VIEIRA VENDRAMINI [FELIPEVIEIRAVENDRAMI]
// 
// Comet.Discord - Comet.Discord.Network - ICipher.cs
// 
// Creation Date: 2021-03-19 16:15
// =================================================================

#region References

using System;
using Comet.Discord.Network.Sockets;

#endregion

namespace Comet.Discord.Network.Security
{
    /// <summary>
    ///     Defines generalized methods for ciphers used by
    ///     <see cref="TcpServerActor" /> and
    ///     <see cref="TcpServerListener{TActor}" /> for encrypting and decrypting
    ///     data to and from the game client. Can be used to switch between ciphers easily for
    ///     seperate states of the game client connection.
    /// </summary>
    public interface ICipher
    {
        /// <summary>Generates keys using key derivation variables.</summary>
        /// <param name="seeds">Initialized seeds for generating keys</param>
        void GenerateKeys(object[] seeds);

        /// <summary>Decrypts data from the client</summary>
        /// <param name="src">Source span that requires decrypting</param>
        /// <param name="dst">Destination span to contain the decrypted result</param>
        void Decrypt(Span<byte> src, Span<byte> dst);

        /// <summary>Encrypts data to send to the client</summary>
        /// <param name="src">Source span that requires encrypting</param>
        /// <param name="dst">Destination span to contain the encrypted result</param>
        void Encrypt(Span<byte> src, Span<byte> dst);
    }
}