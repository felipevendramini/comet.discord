﻿// =================================================================
// Comet.Discord.Bot Project
// 
// This project creates a Discord bot to communicate with Comet Conquer Online Emulator. For other
// related projects see:
// 
// Comet: https://gitlab.com/felipevendramini/comet
// Comet.Updater: https://gitlab.com/felipevendramini/comet.updater
// Comet.Discord: https://gitlab.com/felipevendramini/comet.discord
// 
// User: FELIPE VIEIRA VENDRAMINI [FELIPEVIEIRAVENDRAMI]
// 
// Comet.Discord - Comet.Discord.Bot - Program.cs
// 
// Creation Date: 2021-03-16 19:23
// =================================================================

#region References

using System.Threading.Tasks;

#endregion

namespace Comet.Discord.Bot
{
    public class Program
    {
        public static async Task<int> Main(string[] args)
        {
            BotConfiguration.Config = new BotConfiguration();
            await new Bot().MainAsync().ConfigureAwait(true);
            return 0;
        }
    }
}