﻿// =================================================================
// Comet.Discord.Bot Project
// 
// This project creates a Discord bot to communicate with Comet Conquer Online Emulator. For other
// related projects see:
// 
// Comet: https://gitlab.com/felipevendramini/comet
// Comet.Updater: https://gitlab.com/felipevendramini/comet.updater
// Comet.Discord: https://gitlab.com/felipevendramini/comet.discord
// 
// User: FELIPE VIEIRA VENDRAMINI [FELIPEVIEIRAVENDRAMI]
// 
// Comet.Discord - Comet.Discord.Bot - DbDiscordUser.cs
// 
// Creation Date: 2021-03-17 11:41
// =================================================================

#region References

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

#endregion

namespace Comet.Discord.Bot.Database.Entities
{
    [Table("discord_user")]
    public class DbDiscordUser
    {
        [Key] public uint Identity { get; set; }

        public ulong DiscordUserId { get; set; }
        public uint AccountId { get; set; }
        public uint GameUserId { get; set; }
        public string AccountName { get; set; }
        public string GameName { get; set; }
        public string Name { get; set; }
        public string Discriminator { get; set; }
        public int CreatedAt { get; set; }
        public ulong MessagesSent { get; set; }
        public ulong CharactersSent { get; set; }

        public static async Task<DbDiscordUser> GetAsync(ulong idDiscord)
        {
            await using var ctx = new DatabaseContext();
            return await ctx.DiscordUsers.FirstOrDefaultAsync(x => x.DiscordUserId == idDiscord);
        }
    }
}