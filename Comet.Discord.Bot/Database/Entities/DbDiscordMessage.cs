﻿// =================================================================
// Comet.Discord.Bot Project
// 
// This project creates a Discord bot to communicate with Comet Conquer Online Emulator. For other
// related projects see:
// 
// Comet: https://gitlab.com/felipevendramini/comet
// Comet.Updater: https://gitlab.com/felipevendramini/comet.updater
// Comet.Discord: https://gitlab.com/felipevendramini/comet.discord
// 
// User: FELIPE VIEIRA VENDRAMINI [FELIPEVIEIRAVENDRAMI]
// 
// Comet.Discord - Comet.Discord.Bot - DbDiscordMessage.cs
// 
// Creation Date: 2021-03-17 11:44
// =================================================================

#region References

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

#endregion

namespace Comet.Discord.Bot.Database.Entities
{
    [Table("discord_message")]
    public class DbDiscordMessage
    {
        [Key] public ulong Id { get; set; }

        public ulong UserId { get; set; }
        public string CurrentUserName { get; set; }
        public ulong ChannelId { get; set; }
        public string Message { get; set; }
        public int Timestamp { get; set; }
    }
}