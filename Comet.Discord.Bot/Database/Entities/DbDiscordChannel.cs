﻿// =================================================================
// Comet.Discord.Bot Project
// 
// This project creates a Discord bot to communicate with Comet Conquer Online Emulator. For other
// related projects see:
// 
// Comet: https://gitlab.com/felipevendramini/comet
// Comet.Updater: https://gitlab.com/felipevendramini/comet.updater
// Comet.Discord: https://gitlab.com/felipevendramini/comet.discord
// 
// User: FELIPE VIEIRA VENDRAMINI [FELIPEVIEIRAVENDRAMI]
// 
// Comet.Discord - Comet.Discord.Bot - DbDiscordChannel.cs
// 
// Creation Date: 2021-03-17 12:19
// =================================================================

#region References

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

#endregion

namespace Comet.Discord.Bot.Database.Entities
{
    [Table("discord_channel")]
    public class DbDiscordChannel
    {
        [Key] public uint Id { get; set; }
        public ulong ChannelId { get; set; }
        public string Name { get; set; }
        public int CreatedAt { get; set; }
        public bool Default { get; set; }

        public static async Task<DbDiscordChannel> GetAsync(ulong idChannel)
        {
            await using var ctx = new DatabaseContext();
            return await ctx.DiscordChannels.FirstOrDefaultAsync(x => x.ChannelId == idChannel);
        }

        public static async Task<DbDiscordChannel> GetAsync()
        {
            await using var ctx = new DatabaseContext();
            return await ctx.DiscordChannels.FirstOrDefaultAsync(x => x.Default) ??
                   await ctx.DiscordChannels.FirstOrDefaultAsync();
        }
    }
}