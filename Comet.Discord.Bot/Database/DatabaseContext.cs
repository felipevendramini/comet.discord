﻿// =================================================================
// Comet.Discord.Bot Project
// 
// This project creates a Discord bot to communicate with Comet Conquer Online Emulator. For other
// related projects see:
// 
// Comet: https://gitlab.com/felipevendramini/comet
// Comet.Updater: https://gitlab.com/felipevendramini/comet.updater
// Comet.Discord: https://gitlab.com/felipevendramini/comet.discord
// 
// User: FELIPE VIEIRA VENDRAMINI [FELIPEVIEIRAVENDRAMI]
// 
// Comet.Discord - Comet.Discord.Bot - DatabaseContext.cs
// 
// Creation Date: 2021-03-16 21:06
// =================================================================

#region References

using Comet.Discord.Bot.Database.Entities;
using Microsoft.EntityFrameworkCore;

#endregion

namespace Comet.Discord.Bot.Database
{
    public class DatabaseContext : DbContext
    {
        public virtual DbSet<DbDiscordUser> DiscordUsers { get; set; }
        public virtual DbSet<DbDiscordMessage> DiscordMessages { get; set; }
        public virtual DbSet<DbDiscordChannel> DiscordChannels { get; set; }

        private string ConnectionString =>
            $"server={BotConfiguration.Config.Database.Hostname};database={BotConfiguration.Config.Database.Schema};" +
            $"user={BotConfiguration.Config.Database.Username};password={BotConfiguration.Config.Database.Password}";

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySql(ConnectionString, ServerVersion.AutoDetect(ConnectionString));
        }
    }
}