﻿// =================================================================
// Comet.Discord.Bot Project
// 
// This project creates a Discord bot to communicate with Comet Conquer Online Emulator. For other
// related projects see:
// 
// Comet: https://gitlab.com/felipevendramini/comet
// Comet.Updater: https://gitlab.com/felipevendramini/comet.updater
// Comet.Discord: https://gitlab.com/felipevendramini/comet.discord
// 
// User: FELIPE VIEIRA VENDRAMINI [FELIPEVIEIRAVENDRAMI]
// 
// Comet.Discord - Comet.Discord.Bot - BotConfiguration.cs
// 
// Creation Date: 2021-03-16 20:33
// =================================================================

#region References

using Microsoft.Extensions.Configuration;

#endregion

namespace Comet.Discord.Bot
{
    public class BotConfiguration
    {
        public static BotConfiguration Config;

        public BotConfiguration()
        {
            new ConfigurationBuilder()
                .AddJsonFile("Config.json")
                .Build()
                .Bind(this);
        }

        public string Token { get; set; }
        public DatabaseConfiguration Database { get; set; }

        public class DatabaseConfiguration
        {
            public string Hostname { get; set; }
            public string Schema { get; set; }
            public string Username { get; set; }
            public string Password { get; set; }
            public int Port { get; set; }
        }
    }
}