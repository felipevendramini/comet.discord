﻿// =================================================================
// Comet.Discord.Bot Project
// 
// This project creates a Discord bot to communicate with Comet Conquer Online Emulator. For other
// related projects see:
// 
// Comet: https://gitlab.com/felipevendramini/comet
// Comet.Updater: https://gitlab.com/felipevendramini/comet.updater
// Comet.Discord: https://gitlab.com/felipevendramini/comet.discord
// 
// User: FELIPE VIEIRA VENDRAMINI [FELIPEVIEIRAVENDRAMI]
// 
// Comet.Discord - Comet.Discord.Bot - MessageStatisticsHandler.cs
// 
// Creation Date: 2021-03-16 20:54
// =================================================================

#region References

using System.Threading.Tasks;
using Discord.Commands;
using Discord.WebSocket;

#endregion

namespace Comet.Discord.Bot.CommandHandlers
{
    public sealed class MessageStatisticsHandler : ModuleBase<SocketCommandContext>
    {
        [Command("statistic")]
        [Summary("Query statistics about the current user.")]
        public async Task QueryStatisticAsync(
            [Summary(
                "The identity of the user to get information. If not filled get the user who executed the command.")]
            SocketUser user = null)
        {
            var userInfo = user ?? Context.Client.CurrentUser;
            await Context.Channel.SendMessageAsync("");
        }
    }
}