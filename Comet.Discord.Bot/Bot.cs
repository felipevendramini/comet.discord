﻿// =================================================================
// Comet.Discord.Bot Project
// 
// This project creates a Discord bot to communicate with Comet Conquer Online Emulator. For other
// related projects see:
// 
// Comet: https://gitlab.com/felipevendramini/comet
// Comet.Updater: https://gitlab.com/felipevendramini/comet.updater
// Comet.Discord: https://gitlab.com/felipevendramini/comet.discord
// 
// User: FELIPE VIEIRA VENDRAMINI [FELIPEVIEIRAVENDRAMI]
// 
// Comet.Discord - Comet.Discord.Bot - Bot.cs
// 
// Creation Date: 2021-03-16 19:25
// =================================================================

#region References

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Comet.Discord.Bot.Database;
using Comet.Discord.Bot.Database.Entities;
using Comet.Discord.Shared;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;

#endregion

namespace Comet.Discord.Bot
{
    public class Bot
    {
        private static IServiceProvider _serviceProvider;

        private CancellationToken mCancellationToken = new CancellationToken(false);
        private DiscordSocketClient mClient;
        private CommandService mCommandService;

        public ulong MainChannelId { get; private set; }


        // If any services require the client, or the CommandService, or something else you keep on hand,
        // pass them as parameters into this method as needed.
        // If this method is getting pretty long, you can seperate it out into another file using partials.
        private static IServiceProvider ConfigureServices()
        {
            var map = new ServiceCollection();

            // When all your required services are in the collection, build the container.
            // Tip: There's an overload taking in a 'validateScopes' bool to make sure
            // you haven't made any mistakes in your dependency graph.
            return map.BuildServiceProvider();
        }

        public async Task MainAsync()
        {
            mClient = new DiscordSocketClient();
            mClient.Log += LogAsync;

            mClient.Ready += ClientOnReady;

            mCommandService = new CommandService(new CommandServiceConfig
            {
                CaseSensitiveCommands = true,
                SeparatorChar = ' '
            });
            mCommandService.Log += LogAsync;

            await mCommandService.AddModulesAsync(Assembly.GetEntryAssembly(), _serviceProvider);
            mClient.MessageReceived += OnCommandReceiveAsync;

            _serviceProvider = ConfigureServices();

            await mClient.LoginAsync(TokenType.Bot, BotConfiguration.Config.Token);
            await mClient.StartAsync();

            await Task.Delay(Timeout.Infinite, mCancellationToken);
        }

        private async Task ClientOnReady()
        {
            await mClient.SetGameAsync("World Conquer Online");
            await mClient.SetStatusAsync(UserStatus.DoNotDisturb);

            MainChannelId = (await DbDiscordChannel.GetAsync())?.ChannelId ?? 0;
            if (MainChannelId != 0 && mClient.GetChannel(MainChannelId) is SocketTextChannel channel)
            {
                var embed = new EmbedBuilder
                {
                    Author = new EmbedAuthorBuilder
                    {
                        IconUrl = mClient.CurrentUser.GetAvatarUrl(),
                        Name = mClient.CurrentUser.Username
                    },
                    Timestamp = DateTimeOffset.Now,
                    Color = Color.Default,
                    Description = "",
                    Footer = new EmbedFooterBuilder()
                        .WithText("Comet Discord Bot v1.0.0a"),
                    Title = "Comet[PM] está ONLINE!",
                    Fields = new List<EmbedFieldBuilder>
                    {
                        new EmbedFieldBuilder()
                            .WithName("Apresentação")
                            .WithValue($"Olá Conquistadores!\nMeu nome é Comet[PM] e eu estou aqui para mantê-lo atualizado com as informações dos servidores."),
                        new EmbedFieldBuilder()
                            .WithName("Temporário")
                            .WithValue("Por enquanto, não tenho funções. Apenas registrarei as mensagens enviadas por aqui, mas em breve terei comandos para vocês." +
                                       " Caso tenham sugestões, sintam-se livres para mandar mensagens.")
                        //new EmbedFieldBuilder()
                        //    .WithName("Servidor de Login")
                        //    .WithValue("Global [Online]"),
                        //new EmbedFieldBuilder()
                        //    .WithName("Servidores Conectados")
                        //    .WithValue("Brasil [0/1]")
                    },
                    ThumbnailUrl = "https://worldconquer.online/assets/images/logo.png"
                }.Build();
                await channel.SendMessageAsync("Opa! O bot do servidor está online! Em breve teremos comandos para vocês terem um feedback instantaneo do servidor.",
                    false, embed, RequestOptions.Default, AllowedMentions.None);
            }
        }

        private async Task OnCommandReceiveAsync(SocketMessage arg)
        {
            var message = arg as SocketUserMessage;
            if (message == null)
                return;

            if (message.Author.Id == mClient.CurrentUser.Id || message.Author.IsBot)
                return;

            int pos = 0;
            if (message.HasCharPrefix('/', ref pos) || message.HasMentionPrefix(mClient.CurrentUser, ref pos))
            {
                // Create a Command Context.
                var context = new SocketCommandContext(mClient, message);

                // Execute the command. (result does not indicate a return value, 
                // rather an object stating if the command executed successfully).
                await mCommandService.ExecuteAsync(context, pos, _serviceProvider);
            }

            DbDiscordChannel channel = await DbDiscordChannel.GetAsync(arg.Channel.Id);
            if (channel == null)
            {
                channel = new DbDiscordChannel
                {
                    ChannelId = arg.Channel.Id,
                    Name = arg.Channel.Name,
                    CreatedAt = UnixTimestamp.Timestamp(arg.Channel.CreatedAt.UtcDateTime)
                };
                if (!await BaseRepository.SaveAsync(channel))
                    return;
            }
            else
            {
                if (!channel.Name.Equals(arg.Channel.Name))
                {
                    channel.Name = arg.Channel.Name;
                    await BaseRepository.SaveAsync(channel);
                }
            }

            DbDiscordUser user = await DbDiscordUser.GetAsync(arg.Author.Id);
            if (user == null)
            {
                user = new DbDiscordUser
                {
                    DiscordUserId = arg.Author.Id,
                    Name = arg.Author.Username,
                    Discriminator = arg.Author.Discriminator,
                    CreatedAt = UnixTimestamp.Now(),
                    AccountId = 0,
                    AccountName = "",
                    GameUserId = 0,
                    GameName = "",
                    CharactersSent = 0,
                    MessagesSent = 0
                };

                if (!await BaseRepository.SaveAsync(user))
                    return;
            }
            else
            {
                if (!user.Name.Equals(arg.Author.Username) || !user.Discriminator.Equals(arg.Author.Discriminator))
                {
                    user.Name = arg.Author.Username;
                    user.Discriminator = arg.Author.Discriminator;
                    await BaseRepository.SaveAsync(user);
                }
            }

            user.MessagesSent += 1;
            user.CharactersSent += (ulong) arg.Content.Length;
            await BaseRepository.SaveAsync(user);

            await BaseRepository.SaveAsync(new DbDiscordMessage
            {
                UserId = user.DiscordUserId,
                CurrentUserName = $"{user.Name}#{user.Discriminator}",
                Message = arg.Content,
                ChannelId = channel.ChannelId,
                Timestamp = UnixTimestamp.Now()
            });

            await Log.WriteLogAsync(arg.Channel.Name, LogSeverity.Info,
                $"{user.Name}#{user.Discriminator} on Channel {arg.Channel.Name} >> {arg.Content}");
        }

        public void Cancel()
        {
            mCancellationToken = new CancellationToken(true);
        }

        public Task LogAsync(LogMessage log)
        {
            if (log.Exception != null)
                return Log.WriteLogAsync(LogSeverity.Critical, log.Exception.ToString());
            return Log.WriteLogAsync(log.Severity, log.Message);
        }
    }
}